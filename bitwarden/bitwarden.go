package bitwarden

import (
	"errors"
	"fmt"

	"github.com/docker/docker-credential-helpers/credentials"
)

// Add appends credentials to the store.
func (bw Bitwarden) Add(creds *credentials.Credentials) error {
	// always defer close session, to ensure the sessionKey cannot be used more than once
	defer closeSession()
	if creds == nil || creds.ServerURL == "" || creds.Secret == "" || creds.Username == "" {
		return errors.New("credentials are nil or serverURL, secret or password is not set")
	}

	// if it exists, then we update instead of adding it
	exisitingItem, exists, err := getItem(creds.ServerURL)
	if err != nil {
		return err
	}

	// if it already exists, update it
	if exists {
		return updateItem(exisitingItem.Id, creds.Username, creds.Secret)
	}

	item := resItem{
		Name: creds.ServerURL,
		Login: resLogin{
			Username: creds.Username,
			Password: creds.Secret,
		},
	}

	err = addItem(item)
	if err != nil {
		return err
	}
	return nil
}

// Delete removes credentials from the store.
func (bw Bitwarden) Delete(serverURL string) error {
	// always defer close session, to ensure the sessionKey cannot be used more than once
	defer closeSession()
	item, exists, err := getItem(serverURL)
	if err != nil {
		return err
	}

	if !exists {
		return nil
	}

	return deleteItem(item.Id)
}

// Get retrieves credentials from the store.
// It returns username and secret as strings.
func (bw Bitwarden) Get(serverURL string) (string, string, error) {
	// always defer close session, to ensure the sessionKey cannot be used more than once
	defer closeSession()
	item, exists, err := getItem(serverURL)
	if err != nil {
		return "", "", err
	}

	if !exists {
		return "", "", fmt.Errorf("Could not find entry for server: %s", serverURL)
	}

	return item.Login.Username, item.Login.Password, nil
}

// List returns the stored serverURLs and their associated usernames.
func (bw Bitwarden) List() (map[string]string, error) {
	// always defer close session, to ensure the sessionKey cannot be used more than once
	defer closeSession()
	items, err := getItems()
	if err != nil {
		return nil, err
	}

	list := make(map[string]string)
	for _, item := range items {
		list[item.Name] = item.Login.Username
	}

	return list, nil
}
