package bitwarden

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
)

type pinentry struct {
	cmd *exec.Cmd

	stdin  io.WriteCloser
	stdout io.ReadCloser
	br     *bufio.Reader
}

func newPinentry() (*pinentry, error) {
	p := &pinentry{}

	// try to read BITWARDEN_PINENTRY
	bwPinentry := os.Getenv("DOCKER_BITWARDEN_PINENTRY")
	if bwPinentry == "" {
		bwPinentry = "pinentry"
	}

	p.cmd = exec.Command(bwPinentry)

	var err error
	p.stdin, err = p.cmd.StdinPipe()
	if err != nil {
		return nil, err
	}

	p.stdout, err = p.cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}

	err = p.cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	p.br = bufio.NewReader(p.stdout)

	line, _, err := p.br.ReadLine()
	if err != nil {
		return nil, err
	}
	if !strings.HasPrefix(string(line), "OK") {
		return nil, fmt.Errorf("Failed to get OK from greeting")
	}

	return p, nil
}

func (p *pinentry) Close() error {
	fmt.Fprintf(p.stdin, "bye\n")

	line, _, err := p.br.ReadLine()
	if err != nil {
		return fmt.Errorf("Faied to close pinentry: %w", err)
	}

	if !strings.HasPrefix(string(line), "OK") {
		return fmt.Errorf("Faied to close - did not get OK")
	}

	err = p.stdin.Close()
	if err != nil {
		return err
	}

	err = p.stdout.Close()
	if err != nil {
		return err
	}

	return nil
}

func (p *pinentry) Set(cmd, val string) error {
	fmt.Fprintf(p.stdin, "%s %s\n", cmd, val)

	line, _, err := p.br.ReadLine()
	if err != nil {
		return fmt.Errorf("Faied to set with cmd '%s' and value '%s': %w", cmd, val, err)
	}

	if !strings.HasPrefix(string(line), "OK") {
		return fmt.Errorf("Faied to set with cmd '%s' and value '%s' - did not get OK", cmd, val)
	}

	return nil
}

func (p *pinentry) GetPin() (string, error) {
	fmt.Fprintf(p.stdin, "GETPIN\n")

	pin, _, err := p.br.ReadLine()
	if err != nil {
		return "", fmt.Errorf("Faied to get pin line 1: %w", err)
	}

	if !strings.HasPrefix(string(pin), "D ") {
		return "", errors.New("Error while getting pin")
	}

	status, _, err := p.br.ReadLine()
	if err != nil {
		return "", fmt.Errorf("Faied to get pin line 2: %w", err)
	}

	if !strings.HasPrefix(string(status), "OK") {
		return "", fmt.Errorf("Failed to get pin - did not get OK")
	}

	if !strings.HasPrefix(string(pin), "D") {
		return "", fmt.Errorf("Failed to get pin - did not get D")
	}

	return strings.Replace(string(pin)[2:], "%25", "%", -1), nil
}
