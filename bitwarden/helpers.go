package bitwarden

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

const BITWARDEN_FOLDER = "docker-credential-helper"

type Bitwarden struct{}

var (
	sessionKey string
)

type resFolder struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type resLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type resItem struct {
	Id    string   `json:"id"`
	Name  string   `json:"name"`
	Login resLogin `json:"login"`
}

func addItem(item resItem) error {
	sessionKey, err := getSession()
	if err != nil {
		return err
	}

	folderId, err := unsureFolderExists()
	if err != nil {
		return err
	}

	// get the item template
	getTemplateItem := []string{"--session", sessionKey, "get", "template", "item"}
	outTemplateItemRaw, err := runBitwardenHelper("", getTemplateItem...)
	if err != nil {
		return fmt.Errorf("Failed to get item template with error: %w - %s", err, outTemplateItemRaw)
	}

	getTemplateItemLogin := []string{"--session", sessionKey, "get", "template", "item.login"}
	outTemplateItemLoginRaw, err := runBitwardenHelper("", getTemplateItemLogin...)
	if err != nil {
		return fmt.Errorf("Failed to get item login template with error: %w - %s", err, outTemplateItemLoginRaw)
	}

	// pipe it into jq, and edit it there
	jqPipe := []string{"-c", "-M", fmt.Sprintf(`.name = %q | .folderId = %q | .login = %s | .login.username = %q | .login.password = %q | .login.totp = ""`, item.Name, folderId, outTemplateItemLoginRaw, item.Login.Username, item.Login.Password)}
	outJqRaw, err := runAppHelper("jq", outTemplateItemRaw, jqPipe...)
	if err != nil {
		return fmt.Errorf("Failed to edit with jq with error: %w - %s", err, outJqRaw)
	}

	// encode json as base64
	itemBase64 := base64.StdEncoding.EncodeToString([]byte(outJqRaw))

	out, err := runBitwardenHelper("", "--session", sessionKey, "create", "item", itemBase64)
	if err != nil {
		return fmt.Errorf("Failed to create item with name '%s' with error: %w - %s", item.Name, err, out)
	}

	return nil
}

func getItems() ([]resItem, error) {
	sessionKey, err := getSession()
	if err != nil {
		return nil, err
	}

	folderId, err := unsureFolderExists()
	if err != nil {
		return nil, err
	}

	byteItems, err := runBitwardenHelper("", "--session", sessionKey, "list", "items", "--folderid", folderId)
	if err != nil {
		return nil, err
	}

	var items []resItem
	err = json.Unmarshal([]byte(byteItems), &items)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func getItem(serverURL string) (resItem, bool, error) {
	items, err := getItems()
	if err != nil {
		return resItem{}, false, err
	}

	for _, item := range items {
		if item.Name == serverURL {
			return item, true, nil
		}
	}

	return resItem{}, false, nil
}

// we assume that this already exists
func updateItem(itemId, username, password string) error {
	sessionKey, err := getSession()
	if err != nil {
		return err
	}

	// get the item first
	getCmd := []string{"--session", sessionKey, "get", "item", itemId}
	// pipe it into jq, and edit it there
	jqPipe := []string{"-c", "-M", fmt.Sprintf(`.login.username = %q | .login.password = %q`, username, password)}
	// edit item
	editCmd := []string{"--session", sessionKey, "edit", "item", itemId}

	// get item raw
	outItemRaw, err := runBitwardenHelper("", getCmd...)
	if err != nil {
		return fmt.Errorf("Failed to get item with error: %w - %s", err, outItemRaw)
	}

	outJqRaw, err := runAppHelper("jq", outItemRaw, jqPipe...)
	if err != nil {
		return fmt.Errorf("Failed to edit with jq with error: %w - %s", err, outJqRaw)
	}

	// base64 encode it
	outJqBase64 := base64.StdEncoding.EncodeToString([]byte(outJqRaw))

	outEditRaw, err := runBitwardenHelper(outJqBase64, editCmd...)
	if err != nil {
		return fmt.Errorf("Failed to edit item with error: %w - %s", err, outEditRaw)
	}

	return nil
}

func deleteItem(itemId string) error {
	sessionKey, err := getSession()
	if err != nil {
		return err
	}

	out, err := runBitwardenHelper("", "--session", sessionKey, "delete", "item", itemId)
	if err != nil {
		return fmt.Errorf("Failed to delete item with error: %w - %s", err, out)
	}

	return nil
}

func getSession() (string, error) {
	if sessionKey != "" {
		return sessionKey, nil
	}

	var hasError bool
	var skey string
	tries := 3
	for i := 0; i < tries; i++ {
		pass, err := promptPassword(hasError)
		if err != nil {
			return "", err
		}

		skey, err = runBitwardenHelper(pass, "unlock", "--raw")
		if err != nil {
			if strings.Contains(err.Error(), "Invalid master password") {
				hasError = true
				continue
			}

			return "", fmt.Errorf("Failed to unlock: %w - %s", err, sessionKey)
		}
		break
	}

	if skey == "" {
		return "", fmt.Errorf("Failed to unlock after %d retries", tries)
	}

	sessionKey = skey

	return sessionKey, nil
}

func closeSession() error {
	if sessionKey == "" {
		return nil
	}

	out, err := runBitwardenHelper("", "lock", sessionKey)
	if err != nil {
		return fmt.Errorf("Failed to lock, with the sessionKey: %w - %s - ", err, out)
	}

	return nil
}

func bitwardenFolder() string {
	bwFolder := os.Getenv("DOCKER_BITWARDEN_FOLDER")
	if bwFolder != "" {
		return bwFolder
	}

	return BITWARDEN_FOLDER
}

// checks for the folder bitwardenFolder(), and returns the id of it
func folderExists() (string, error) {
	sessionKey, err := getSession()
	if err != nil {
		return "", err
	}

	folderData, err := runBitwardenHelper("", "--session", sessionKey, "get", "folder", bitwardenFolder())
	if err != nil {
		return "", err
	}

	// parse output, return id
	var folder resFolder
	err = json.Unmarshal([]byte(folderData), &folder)
	if err != nil {
		return "", err
	}

	if folder.Id == "" {
		return "", fmt.Errorf("Could not find any id for the return folder, looking for: %s", bitwardenFolder())
	}

	return folder.Id, nil
}

// chocks if folder exists, if not it creates it
func unsureFolderExists() (string, error) {
	sessionKey, err := getSession()
	if err != nil {
		return "", err
	}

	// if exists, return it
	folderId, err := folderExists()
	if err != nil {
		return "", err
	}

	if folderId != "" {
		return folderId, nil
	}

	folderBytes, err := json.Marshal(resFolder{Name: bitwardenFolder()})
	if err != nil {
		return "", err
	}

	// encode json as base64
	folderBase64 := base64.StdEncoding.EncodeToString(folderBytes)

	createdFolderBytes, err := runBitwardenHelper("", "--session", sessionKey, "create", "folder", folderBase64)
	if err != nil {
		return "", err
	}

	var folder resFolder
	err = json.Unmarshal([]byte(createdFolderBytes), &folder)
	if err != nil {
		return "", err
	}

	if folder.Id == "" {
		return "", fmt.Errorf("Could not create folder with name '%s'", bitwardenFolder())
	}

	return folder.Id, nil
}

func promptPassword(hasErr bool) (string, error) {
	p, err := newPinentry()
	if err != nil {
		return "", err
	}

	must := func(err error) {
		if err != nil {
			log.Fatal(err)
		}
	}

	must(p.Set("SETDESC", "Docker Credential Helpers - BitWarden"))
	must(p.Set("SETPROMPT", "Bitwarden master key"))
	must(p.Set("SETOK", "Unlock"))
	must(p.Set("SETCANCEL", "Cancel"))

	if hasErr {
		must(p.Set("SETERROR", "Invalid master key entered - please try again"))
	}

	pin, err := p.GetPin()
	if err != nil {
		return "", err
	}

	return pin, nil
}

func runBitwardenHelper(stdin string, args ...string) (string, error) {
	return runAppHelper("bw", stdin, args...)
}

func runAppHelper(app, stdin string, args ...string) (string, error) {
	var stdout, stderr bytes.Buffer
	cmd := exec.Command(app, args...)
	cmd.Stdin = strings.NewReader(stdin)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	if err != nil {
		return "", fmt.Errorf("%w: %s", err, stderr.String())
	}

	return strings.TrimRight(stdout.String(), "\n\r"), nil
}
