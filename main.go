package main

import (
	"github.com/docker/docker-credential-helpers/credentials"
	"gitlab.com/eyJhb/docker-credential-bitwarden/bitwarden"
)

func main() {
	credentials.Serve(bitwarden.Bitwarden{})
}
