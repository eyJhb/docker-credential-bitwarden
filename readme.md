# Docker Credential Helper for Bitwarden
This adds support to use Bitwarden as a credential helper for Docker.
By default Docker will save usernames/passwords as plaintext, which is not a desired behaviour.

This helper will save all entries to a folder name `docker-credential-helper` by default, unless a in environment variable `DOCKER_BITWARDEN_FOLDER` is set.

Default it will ask for pins using `pinentry` executable, but can be set to another pinentry program by setting the `DOCKER_BITWARDEN_PINENTRY` environment variable as well.
Keep in mind, it needs to support what is listed [here](https://gist.github.com/mdeguzis/05d1f284f931223624834788da045c65).
For what functionality is used, just read the pinentry file.

## Requirements
- bitwarden-cli `bw`
- jq
- pinentry

## Notes
This does not perform a sync for you, and you are required to do such yourself.

Enjoy!
